#include "Preprocessor.h"

using namespace cv;
using namespace std;

Preprocessor::Preprocessor(string input)
{
	filepath = input;
}


Preprocessor::~Preprocessor()
{

}

inline double Preprocessor::gauss(double x, double mean, double sigma)
{
	return (exp((-(x - mean)*(x - mean)) / (2 * sigma*sigma)) / sqrt(PI * 2.0 * sigma * sigma));
}

void Preprocessor::create_dir()
{

}

int Preprocessor::generate_lineart()
{
	Mat pigray, img_color, img_color_c, img_show;
	//input image select
	img_color_c = imread(filepath, 1);
	if (img_color_c.empty())
		return 0;
	img_color_c.convertTo(img_color_c, CV_32FC3, 1.0 / 255);
	cvtColor(img_color_c, pigray, COLOR_BGR2GRAY);
	img_color = img_color_c.clone();
	img_show = img_color_c.clone();

	int COLS = pigray.cols;
	int ROWS = pigray.rows;

	Mat grad_rx, grad_ry, grad_gx, grad_gy, grad_bx, grad_by, gradx, grady;
	//Sobel filter----------------------------------------------------------------
	Mat channels[3];
	vector<Mat> grad_x, grad_y;
	split(img_color, channels);
	//R
	Sobel(channels[2], grad_rx, CV_32FC1, 1, 0, sobel_k);
	Sobel(channels[2], grad_ry, CV_32FC1, 0, 1, sobel_k);
	grad_x.push_back(grad_rx);
	grad_y.push_back(grad_ry);
	//G
	Sobel(channels[1], grad_gx, CV_32FC1, 1, 0, sobel_k);
	Sobel(channels[1], grad_gy, CV_32FC1, 0, 1, sobel_k);
	grad_x.push_back(grad_gx);
	grad_y.push_back(grad_gy);
	//B
	Sobel(channels[0], grad_bx, CV_32FC1, 1, 0, sobel_k);
	Sobel(channels[0], grad_by, CV_32FC1, 0, 1, sobel_k);
	grad_x.push_back(grad_bx);
	grad_y.push_back(grad_by);
	//combine
	merge(grad_x, gradx);
	merge(grad_y, grady);
	//dot -> structure tensor elements & gaussian
	Mat strt;
	vector<Mat> st;
	Mat stE = Mat::zeros(pigray.size(), CV_32FC1);
	Mat stG = Mat::zeros(pigray.size(), CV_32FC1);
	Mat stF = Mat::zeros(pigray.size(), CV_32FC1);
#pragma omp parallel for
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Vec3f st_e = gradx.at<Vec3f>(i, j);
			Vec3f st_g = grady.at<Vec3f>(i, j);
			stE.at<float>(i, j) = st_e.dot(st_e);
			stG.at<float>(i, j) = st_g.dot(st_g);
			stF.at<float>(i, j) = st_e.dot(st_g);
		}
	}
	st.push_back(stE);
	st.push_back(stG);
	st.push_back(stF);
	merge(st, strt);

	GaussianBlur(strt, strt, Size(9, 9), 0, 0);
	GaussianBlur(strt, strt, Size(9, 9), 0, 0);
	//eigenvector -> tangent
	Mat eigen_v2 = Mat::zeros(pigray.size(), CV_32FC3);
#pragma omp parallel for
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Vec3f st_r = strt.at<Vec3f>(i, j);
			float sq_e = (st_r[0] - st_r[1]);
			float sq = sqrt(sq_e*sq_e + 4 * st_r[2] * st_r[2]);
			float lam1_ = 0.5*(st_r[0] + st_r[1] + sq);
			eigen_v2.at<Vec3f>(i, j) = normalize(Vec3f(st_r[0] - lam1_, st_r[2], 0));
		}
	}

	//Oriented-Bilateral-Filter---------------------------------------------------------
	//RGB -> CIE-Lab
	Mat img_color_lab, bf_output;
	cvtColor(img_color_c, img_color_lab, CV_BGR2Lab);
	Mat bf_temp = Mat::zeros(pigray.size(), CV_32FC3);
	Mat bf_out = Mat::zeros(pigray.size(), CV_32FC3);
	double sigma_r = 4.25 * ROWS / 500;
#pragma omp parallel for
	for (int n = 0; n < iterate; n++) {
		//gradient
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				Vec3f tan_temp = eigen_v2.at<Vec3f>(i, j);
				Vec3f tan_v1 = Vec3f(tan_temp[1], -tan_temp[0], 0);
				Vec3f center = img_color_lab.at<Vec3f>(i, j);
				if (tan_temp[0] == 0 && tan_temp[1] == 0) {
					bf_temp.at<Vec3f>(i, j) = center;
					continue;
				}
				Vec3f tan_t0 = (abs(tan_v1[0]) > abs(tan_v1[1])) ? Vec3f(1, tan_v1[1] / tan_v1[0], 0) : Vec3f(tan_v1[0] / tan_v1[1], 1, 0);
				float len_t0 = norm(tan_t0);
				Vec3f tan_sum = gauss(0.0, 0.0, sigma_d)*gauss(0.0, 0.0, sigma_r) * 2 * center;
				float tan_norm = gauss(0.0, 0.0, sigma_d)*gauss(0.0, 0.0, sigma_r) * 2;
				int num_d = floor(2 * sigma_d / len_t0);
				for (float d = 1; d <= num_d; d++) {
					double row_0 = i + tan_t0[1] * d;
					double col_0 = j + tan_t0[0] * d;
					double row_1 = i - tan_t0[1] * d;
					double col_1 = j - tan_t0[0] * d;
					Vec3f value_0, value_1;
					if (col_0 > (double)COLS - 1 || col_0 < 0.0 || row_0 >(double)ROWS - 1 || row_0 < 0.0) {
						value_0 = center;
					}
					else {
						value_0 = img_color_lab.at<Vec3f>((int)round(row_0), (int)round(col_0));
					}
					if (col_1 >(double)COLS - 1 || col_1 < 0.0 || row_1 >(double)ROWS - 1 || row_1 < 0.0) {
						value_1 = center;
					}
					else {
						value_1 = img_color_lab.at<Vec3f>((int)round(row_1), (int)round(col_1));
					}
					float e0 = norm(value_0 - center);
					float e1 = norm(value_1 - center);
					double kernel_d = gauss(d*len_t0, 0.0, sigma_d);
					double kernel_0 = gauss(e0, 0.0, sigma_r);
					double kernel_1 = gauss(e1, 0.0, sigma_r);
					tan_norm += kernel_d * (kernel_0 + kernel_1);
					tan_sum += kernel_d * kernel_0 * value_0;
					tan_sum += kernel_d * kernel_1 * value_1;
				}
				bf_temp.at<Vec3f>(i, j) = tan_sum / tan_norm;
			}
		}
		//tangent
		for (int i = 0; i < ROWS; i++) {
			for (int j = 0; j < COLS; j++) {
				Vec3f tan_v2 = eigen_v2.at<Vec3f>(i, j);
				Vec3f center = bf_temp.at<Vec3f>(i, j);
				if (tan_v2[0] == 0 && tan_v2[1] == 0) {
					bf_out.at<Vec3f>(i, j) = center;
					continue;
				}
				Vec3f tan_t0 = (abs(tan_v2[0]) > abs(tan_v2[1])) ? Vec3f(1, tan_v2[1] / tan_v2[0], 0) : Vec3f(tan_v2[0] / tan_v2[1], 1, 0);
				float len_t0 = norm(tan_t0);
				Vec3f tan_sum = gauss(0.0, 0.0, sigma_d)*gauss(0.0, 0.0, sigma_r) * 2 * center;
				float tan_norm = gauss(0.0, 0.0, sigma_d)*gauss(0.0, 0.0, sigma_r) * 2;
				int num_d = floor(2 * sigma_d / len_t0);
				for (float d = 1; d <= num_d; d++) {
					double row_0 = i + tan_t0[1] * d;
					double col_0 = j + tan_t0[0] * d;
					double row_1 = i - tan_t0[1] * d;
					double col_1 = j - tan_t0[0] * d;
					Vec3f value_0, value_1;
					if (col_0 > (double)COLS - 1 || col_0 < 0.0 || row_0 >(double)ROWS - 1 || row_0 < 0.0) {
						value_0 = center;
					}
					else {
						value_0 = bf_temp.at<Vec3f>((int)round(row_0), (int)round(col_0));
					}
					if (col_1 >(double)COLS - 1 || col_1 < 0.0 || row_1 >(double)ROWS - 1 || row_1 < 0.0) {
						value_1 = center;
					}
					else {
						value_1 = bf_temp.at<Vec3f>((int)round(row_1), (int)round(col_1));
					}
					float e0 = norm(value_0 - center);
					float e1 = norm(value_1 - center);
					double kernel_d = gauss(d*len_t0, 0.0, sigma_d);
					double kernel_0 = gauss(e0, 0.0, sigma_r);
					double kernel_1 = gauss(e1, 0.0, sigma_r);
					tan_norm += kernel_d * (kernel_0 + kernel_1);
					tan_sum += kernel_d * kernel_0 * value_0;
					tan_sum += kernel_d * kernel_1 * value_1;
				}
				bf_out.at<Vec3f>(i, j) = tan_sum / tan_norm;
			}
		}
		img_color_lab = bf_out.clone();
	}
	bf_output = bf_out.clone();
	vector<Mat> planes;
	split(bf_output, planes);

	//FDoG------------------------------------------------------
	Mat fdog_map_temp = Mat::zeros(pigray.size(), CV_32FC3);
	Mat fdog_map_out = Mat::zeros(pigray.size(), CV_32FC1);
#pragma omp parallel for
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Vec3f flow_temp = eigen_v2.at<Vec3f>(i, j);
			Vec3f flow = Vec3f(flow_temp[1], -flow_temp[0], 0);
			if (flow[0] == 0 && flow[1] == 0) continue;
			float flow_len = (abs(flow[0]) > abs(flow[1])) ? 1 / abs(flow[0]) : 1 / abs(flow[1]);
			int num_d = floor(2 * sigma_s / flow_len);
			Vec3f center = bf_output.at<Vec3f>(i, j);
			Vec3f sum_fdog = Vec3f(gauss(0.0, 0.0, sigma_e)*center[0], gauss(0.0, 0.0, sigma_s)*center[0], 0);
			Vec3f norm_fdog = Vec3f(gauss(0.0, 0.0, sigma_e), gauss(0.0, 0.0, sigma_s), 0.0);
			for (int k = 1; k <= num_d; k++) {
				Vec3f kernel = Vec3f(gauss(k*flow_len, 0.0, sigma_e), gauss(k*flow_len, 0.0, sigma_s), 0.0);
				norm_fdog += 2.0*kernel;
				double row_0 = i + flow[1] * k * flow_len;
				double col_0 = j + flow[0] * k * flow_len;
				double row_1 = i - flow[1] * k * flow_len;
				double col_1 = j - flow[0] * k * flow_len;
				Vec3f temp_0, temp_1;
				if (col_0 > (double)COLS - 1 || col_0 < 0.0 || row_0 >(double)ROWS - 1 || row_0 < 0.0) {
					temp_0 = center;
				}
				else {
					temp_0 = bf_output.at<Vec3f>((int)round(row_0), (int)round(col_0));
				}
				if (col_1 >(double)COLS - 1 || col_1 < 0.0 || row_1 >(double)ROWS - 1 || row_1 < 0.0) {
					temp_1 = center;
				}
				else {
					temp_1 = bf_output.at<Vec3f>((int)round(row_1), (int)round(col_1));
				}
				Vec3f L0 = Vec3f(temp_0[0], temp_0[0], 0);
				Vec3f L1 = Vec3f(temp_1[0], temp_1[0], 0);
				Vec3f L_temp = L0 + L1;
				sum_fdog += Vec3f(kernel[0] * L_temp[0], kernel[1] * L_temp[1], 0.0);
			}
			Vec3f sum_temp = Vec3f(sum_fdog[0] / norm_fdog[0], sum_fdog[1] / norm_fdog[1], 0);
			fdog_map_temp.at<Vec3f>(i, j) = Vec3f((sum_temp[0] - rho * sum_temp[1]) * 100, center[1], center[2]);
		}
	}
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Vec3f h_t = eigen_v2.at<Vec3f>(i, j);
			float sum_h = alpha * gauss(0.0, 0.0, sigma_m) * fdog_map_temp.at<Vec3f>(i, j)[0];
			float w = alpha * gauss(0.0, 0.0, sigma_m);//1.0;
			Point2f a_p = (j, i);
			Point2f b_p = (j, i);
			Vec3f a_t = h_t;
			Vec3f b_t = -h_t;
			float a_w = 0.0;
			float b_w = 0.0;
			float half_h = 2.0 * sigma_m;
			float a_dw = 0.0;
			float b_dw = 0.0;

			if (h_t[0] == 0 && h_t[1] == 0) {
				fdog_map_out.at<float>(i, j) = 1.0;
				continue;
			}
			Vec3f t = h_t;
			while (a_w < half_h) {
				if (t[0] == 0 && t[1] == 0) {
					a_w = half_h;
					continue;
				}
				if (t.dot(a_t) < 0) t = -t;
				a_t = t;
				float sign_x, sign_y;
				if (t[0] > 0) { sign_x = 1; }
				else if (t[0] < 0) { sign_x = -1; }
				else { sign_x = 0; }
				if (t[1] > 0) { sign_y = 1; }
				else if (t[1] < 0) { sign_y = -1; }
				else { sign_y = 0; }
				float a_dw_temp = a_dw;
				a_dw = (abs(t[0]) > abs(t[1])) ? abs((a_p.x - floorf(a_p.x) - 0.5 - sign_x) / t[0]) : abs((a_p.y - floorf(a_p.y) - 0.5 - sign_y) / t[1]);
				a_p += Point2f(t[0] * a_dw, t[1] * a_dw);
				a_w += a_dw;
				//
				float k = 0.5 * (a_dw + a_dw_temp) * gauss(a_w, 0.0, sigma_m);
				if (a_p.x > (double)COLS - 1 || a_p.x < 0.0 || a_p.y >(double)ROWS - 1 || a_p.y < 0.0) continue;
				sum_h += k * fdog_map_temp.at<Vec3f>(round(a_p.y), round(a_p.x))[0];
				w += k;
				t = eigen_v2.at<Vec3f>(round(a_p.y), round(a_p.x));
			}
			Vec3f t2 = h_t;
			while (b_w < half_h) {
				if (t2[0] == 0 && t2[1] == 0) {
					b_w = half_h;
					continue;
				}
				if (t2.dot(b_t) < 0) t2 = -t2;
				b_t = t2;
				float sign_x, sign_y;
				if (t2[0] > 0) { sign_x = 1; }
				else if (t2[0] < 0) { sign_x = -1; }
				else { sign_x = 0; }
				if (t2[1] > 0) { sign_y = 1; }
				else if (t2[1] < 0) { sign_y = -1; }
				else { sign_y = 0; }
				float b_dw_temp = b_dw;
				b_dw = (abs(t2[0]) > abs(t2[1])) ? abs((b_p.x - floorf(b_p.x) - 0.5 - sign_x) / t2[0]) : abs((b_p.y - floorf(b_p.y) - 0.5 - sign_y) / t2[1]);
				b_p += Point2f(t2[0] * b_dw, t2[1] * b_dw);
				b_w += b_dw;
				//
				float k = 0.5 * (b_dw + b_dw_temp) * gauss(b_w, 0.0, sigma_m);
				if (b_p.x > (double)COLS - 1 || b_p.x < 0.0 || b_p.y >(double)ROWS - 1 || b_p.y < 0.0) continue;
				sum_h += k * fdog_map_temp.at<Vec3f>(round(b_p.y), round(b_p.x))[0];
				w += k;
				t2 = eigen_v2.at<Vec3f>(round(b_p.y), round(b_p.x));
			}
			float h = sum_h / w;
			float edge_1 = min(max((phi*h + 2.0) / 4.0, 0.0), 1.0);
			float edge = (h > 0) ? 1.0 : 2.0*edge_1*edge_1*(3 - 2 * edge_1); //  (h > 0) ? 1.0 : 1.0 + tanh(phi*h);
			fdog_map_out.at<float>(i, j) = edge; //(h < 0 || edge < tau) ? 0.0 : 1.0;
		}
	}
	threshold(fdog_map_out, fdog_map_out, tau, 1.0, THRESH_BINARY);
	normalize(fdog_map_out, fdog_map_out, 0, 255, NORM_MINMAX);
	imwrite("fdog.png", fdog_map_out);
	//for color line generation---------------------------------------------------
	Mat fdog_map_color = Mat::ones(pigray.size(), CV_32FC3);
#pragma omp parallel for
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			float edge_value = fdog_map_out.at<float>(i, j);
			if (edge_value == 0.0) {
				fdog_map_color.at<Vec3f>(i, j) = img_color.at<Vec3f>(i, j);
			}
			else {
				fdog_map_color.at<Vec3f>(i, j) = Vec3f(1.0, 1.0, 1.0);
			}
		}
	}
	normalize(fdog_map_color, fdog_map_color, 0, 255, NORM_MINMAX);
	imwrite("color.png", fdog_map_color);
	//----------------------------------------------------------------------------

	//draw arrowed line-----------------------------------------------------------
#pragma omp parallel for
	for (int i = 0; i < ROWS; i += 10) {
		for (int j = 0; j < COLS; j += 10) {
			Vec3f v = eigen_v2.at<Vec3f>(i, j);
			Point2f p(j, i);
			Point2f p2(j + v[0] * 10, i + v[1] * 10);
			arrowedLine(img_color, p, p2, Scalar(255, 0, 0), 1.5, 8, 0, 0.3);
		}
	}
	//----------------------------------------------------------------------------

	//Line Integral Convolution---------------------------------------------------
	Mat noise = Mat::zeros(Size(COLS / 2, ROWS / 2), CV_32FC1);
	Mat LIC = Mat::zeros(pigray.size(), CV_32FC1);
	randu(noise, 0, 1.0f);
	resize(noise, noise, pigray.size(), 0, 0, INTER_NEAREST);

	int s = 10;
	float sigma = 2 * s*s;

#pragma omp parallel for
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			float w_sum = 0.0;
			float x = j;
			float y = i;
			for (int k = 0; k < s; k++) {
				Vec3f v = eigen_v2.at<Vec3f>(int(y + ROWS) % ROWS, int(x + COLS) % COLS);
				if (v[0] != 0) x = x + (abs(v[0]) / float(abs(v[0]) + abs(v[1])))*(abs(v[0]) / v[0]);
				if (v[1] != 0) y = y + (abs(v[1]) / float(abs(v[0]) + abs(v[1])))*(abs(v[1]) / v[1]);
				float r2 = k * k;
				float w = (1 / (PI*sigma))*exp(-(r2) / sigma);
				int yy = (int(y) + ROWS) % ROWS;
				int xx = (int(x) + COLS) % COLS;
				LIC.at<float>(i, j) += w * noise.at<float>(yy, xx);
				w_sum += w;
			}

			x = j;
			y = i;
			for (int k = 0; k < s; k++) {
				Vec3f v = -eigen_v2.at<Vec3f>(int(y + ROWS) % ROWS, int(x + COLS) % COLS);
				if (v[0] != 0) x = x + (abs(v[0]) / float(abs(v[0]) + abs(v[1])))*(abs(v[0]) / v[0]);
				if (v[1] != 0) y = y + (abs(v[1]) / float(abs(v[0]) + abs(v[1])))*(abs(v[1]) / v[1]);

				float r2 = k * k;
				float w = (1 / (PI*sigma))*exp(-(r2) / sigma);
				LIC.at<float>(i, j) += w * noise.at<float>(int(y + ROWS) % ROWS, int(x + COLS) % COLS);
				w_sum += w;
			}
			LIC.at<float>(i, j) /= w_sum;
		}
	}
	//-------------------------------------------------------------------------------------------------
	cvtColor(bf_out, bf_out, CV_Lab2BGR);
	normalize(bf_out, bf_out, 0, 255, NORM_MINMAX);
	imwrite("bf.png", bf_out);
	normalize(img_color, img_color, 0, 255, NORM_MINMAX);
	normalize(LIC, LIC, 0, 255, NORM_MINMAX);
	imwrite("draw vector.png", img_color);
	imwrite("LIC.png", LIC);

	waitKey(0);

	return 0;
}

void Preprocessor::lighten_image()
{
	Mat image = imread(filepath);

}
