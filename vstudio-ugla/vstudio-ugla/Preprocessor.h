#pragma once

#include <opencv2/core.hpp> 
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/opencv.hpp>
#include <sys/types.h>
#include <sys/stat.h>
#include <math.h>
#include <iostream>
#include <string>

#define sobel_k 5
#define PI 3.1415926535
#define iterate 5        //3-
#define SIGMA_RATIO 1.6 //Fixed
#define sigma_m 1.0    //1.0-3.0
#define sigma_e 1.0   //0.7-
#define rho 0.99     //0.97-0.992
#define alpha 5.0   //2.0-5.0
#define phi 0.07   //0.01-0.1
#define tau 0.5   //0.1-0.5
#define sigma_s SIGMA_RATIO*sigma_e
#define sigma_d  3

class Preprocessor
{
public:
	Preprocessor(std::string input);
	~Preprocessor();
	inline double gauss(double x, double mean, double sigma);
	void create_dir();
	int generate_lineart();
	void lighten_image();
private:
	std::string filepath;
};

