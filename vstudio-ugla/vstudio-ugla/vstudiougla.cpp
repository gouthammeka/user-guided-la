﻿#include "vstudiougla.h"
#include<QtWidgets>
#include<iostream>
#include<string>

vstudiougla::vstudiougla(): QMainWindow()
{
	setWindowTitle("UGLA");

	title = new QLabel(this);
	title->setText("User-Guided Line Abstraction");
	title->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
	title->setGeometry(0, 0, 1280, 60);
	QFont font_title = title->font();
	font_title.setPointSize(20);
	font_title.setBold(true);
	title->setFont(font_title);

	description = new QLabel(this);
	description->setFrameStyle(QFrame::Panel);
	description->setText("This program takes an input of a .JPG/.PNG image file, takes graphical input from the user and generates a line abstraction of the original image as output.");
	description->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
	description->setGeometry(20, 60, 1000, 40);

	label1 = new QLabel(this);
	label1->setFrameStyle(QFrame::Panel);
	QFont font_image_label = label1->font();
	font_image_label.setBold(true);
	label1->setFont(font_image_label);
	label1->setText(" ");
	label1->setAlignment(Qt::AlignTop | Qt::AlignHCenter);
	label1->setGeometry(20, 120, 610, 520);

	label2 = new QLabel(this);
	label2->setFrameStyle(QFrame::Panel);
	label2->setText(" ");
	label2->setAlignment(Qt::AlignTop | Qt::AlignHCenter);
	label2->setGeometry(650, 120, 610, 520);
	label2->setFont(font_image_label);

	statusLabel = new QLabel(this);
	statusLabel->setText(" ");
	statusLabel->setAlignment(Qt::AlignCenter | Qt::AlignHCenter);
	statusLabel->setGeometry(260, 660, 520, 40);

	imglabel1 = new QLabel(this);
	label1->setFrameStyle(QFrame::Panel);
	imglabel1->setBackgroundRole(QPalette::Base);
	imglabel1->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imglabel1->setScaledContents(true);
	imglabel1->setVisible(false);

	imglabel2 = new QLabel(this);
	label2->setFrameStyle(QFrame::Panel);
	imglabel2->setBackgroundRole(QPalette::Base);
	imglabel2->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	imglabel2->setScaledContents(true);
	imglabel2->setVisible(false);

	src_area = new QScrollArea(this);
	src_area->setBackgroundRole(QPalette::Dark);
	src_area->setWidget(imglabel1);
	src_area->setVisible(true);
	src_area->setGeometry(30, 150, 590, 480);

	final_area = new QScrollArea(this);
	final_area->setBackgroundRole(QPalette::Dark);
	final_area->setWidget(imglabel2);
	final_area->setVisible(true);
	final_area->setGeometry(660, 150, 590, 480);

	load_btn = new QPushButton("画像を読み込む", this); // 画像を読み込む == Load Image
	load_btn->setGeometry(1040, 60, 220, 40);
	connect(load_btn, SIGNAL(clicked()), this, SLOT(load_img()));

	reset_btn = new QPushButton("リセット", this); // リセット == Reset
	reset_btn->setGeometry(20, 660, 220, 40);
	reset_btn->setEnabled(false);
	connect(reset_btn, SIGNAL(clicked()), this, SLOT(reset()));

	next_btn = new QPushButton("Generate Line Art", this); 
	next_btn->setGeometry(1040, 660, 220, 40);
	next_btn->setEnabled(false);
	connect(next_btn, SIGNAL(clicked()), this, SLOT(generate_lineart()));

	clear_btn = new QPushButton("Clear Paint", this); 
	clear_btn->setGeometry(800, 660, 220, 40);
	clear_btn->setEnabled(false);

	fit1_btn = new QPushButton("フィット", this); // フィット == Fit
	fit1_btn->setGeometry(570, 130, 50, 20);
	fit1_btn->setCheckable(true);
	fit1_btn->setEnabled(false);
	connect(fit1_btn, SIGNAL(clicked()), this, SLOT(fit_img1()));

	fit2_btn = new QPushButton("フィット", this); // フィット == Fit
	fit2_btn->setGeometry(1190, 130, 50, 20);
	fit2_btn->setCheckable(true);
	fit2_btn->setEnabled(false);
	connect(fit2_btn, SIGNAL(clicked()), this, SLOT(fit_img2()));
}

void vstudiougla::load_img()
{
	QString filename = QFileDialog::getOpenFileName(this, tr("イメージを開く"), QDir::currentPath(), tr("イメージファイル (*.png *.jpg)"));
	if (filename != NULL) 
	{
		description->setText("The current image file loaded is: " + filename);
		label1->setText("Source Image"); // 
		Pprocessor = new Preprocessor(filename.toStdString());
		load_btn->setEnabled(false);
		reset_btn->setEnabled(true);
		next_btn->setEnabled(true);
		fit1_btn->setEnabled(true);
		src_img = new QPixmap(filename);
		imglabel1->setGeometry(30, 150, src_img->width(), src_img->height());
		imglabel1->setVisible(true);
		imglabel1->setPixmap(*src_img);
		statusLabel->setText("Image has been loaded.");
	}
}

void vstudiougla::reset()
{
	load_btn->setEnabled(true);
	reset_btn->setEnabled(false);
	description->setText("This program takes an input of a .JPG/.PNG image file, takes graphical input from the user and generates a line abstraction of the original image as output.");
}

void vstudiougla::fit_img1()
{
	int x, y;
	if (fit1_btn->isChecked())
	{
		if (src_img->width() > src_img->height())
		{
			x = 585;
			y = src_img->height() * x / src_img->width();
		}
		else 
		{
			y = 475;
			x = src_img->width() * y / src_img->height();
		}
		imglabel1->setGeometry(0, 0, x, y);
	}
	else
	{
		imglabel1->setGeometry(0, 0, src_img->width(), src_img->height());
	}
}

void vstudiougla::fit_img2()
{
	int x, y;
	if (fit2_btn->isChecked())
	{
		if (final_img->width() > final_img->height())
		{
			x = 585;
			y = final_img->height() * x / final_img->width();
		}
		else
		{
			y = 475;
			x = final_img->width() * y / final_img->height();
		}
		imglabel2->setGeometry(0, 0, x, y);
	}
	else
	{
		imglabel2->setGeometry(0, 0, final_img->width(), final_img->height());
	}
}

void vstudiougla::generate_lineart()
{
	next_btn->setEnabled(false);
	Pprocessor->generate_lineart();
	statusLabel->setText("Line art generated.");
	final_img = new QPixmap("fdog.png");
	imglabel2->setGeometry(30, 150, final_img->width(), final_img->height());
	imglabel2->setVisible(true);
	imglabel2->setPixmap(*final_img);
	fit2_btn->setEnabled(true);
	next_btn->setText("Draw Abstractions");
	next_btn->setEnabled(true);
	connect(next_btn, SIGNAL(clicked()), this, SLOT(start_draw()));
}

void vstudiougla::start_draw()
{

}
