
#include "vstudiougla.h"
#include <QDesktopWidget>
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);
	vstudiougla window;
	window.setFixedSize(1280, 720);
	window.show();
	return app.exec();
}
