/********************************************************************************
** Form generated from reading UI file 'vstudiougla.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_VSTUDIOUGLA_H
#define UI_VSTUDIOUGLA_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_vstudiouglaClass
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *vstudiouglaClass)
    {
        if (vstudiouglaClass->objectName().isEmpty())
            vstudiouglaClass->setObjectName(QStringLiteral("vstudiouglaClass"));
        vstudiouglaClass->resize(600, 400);
        menuBar = new QMenuBar(vstudiouglaClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        vstudiouglaClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(vstudiouglaClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        vstudiouglaClass->addToolBar(mainToolBar);
        centralWidget = new QWidget(vstudiouglaClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        vstudiouglaClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(vstudiouglaClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        vstudiouglaClass->setStatusBar(statusBar);

        retranslateUi(vstudiouglaClass);

        QMetaObject::connectSlotsByName(vstudiouglaClass);
    } // setupUi

    void retranslateUi(QMainWindow *vstudiouglaClass)
    {
        vstudiouglaClass->setWindowTitle(QApplication::translate("vstudiouglaClass", "vstudiougla", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class vstudiouglaClass: public Ui_vstudiouglaClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_VSTUDIOUGLA_H
