#pragma once

#include <QtWidgets/QMainWindow>
#include<QtWidgets>
#include "ui_vstudiougla.h"
#include "Preprocessor.h"

class vstudiougla : public QMainWindow
{
	Q_OBJECT

public:
	vstudiougla();

public slots:
	void load_img();
	void reset();
	void fit_img1();
	void fit_img2();
	void generate_lineart();
	void start_draw();

private:
	QPushButton * load_btn;
	QPushButton * reset_btn;
	QPushButton * next_btn;
	QPushButton * clear_btn;
	QPushButton * fit1_btn;
	QPushButton * fit2_btn;
	QLabel * title;
	QLabel * description;
	QLabel * label1;
	QLabel * label2;
	QLabel * imglabel1;
	QLabel * imglabel2;
	QLabel * statusLabel;
	QScrollArea * src_area;
	QScrollArea * final_area;
	QPixmap * src_img;
	QPixmap * final_img;
	QFileDialog * fileDialog;
	Preprocessor * Pprocessor;


};
